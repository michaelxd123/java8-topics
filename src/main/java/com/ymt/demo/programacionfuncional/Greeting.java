package com.ymt.demo.programacionfuncional;

interface MyGreeting {
    String processName(String str);
}