package com.ymt.demo.programacionfuncional;

interface MyGeneric<T> {
    T compute(T t);
}
