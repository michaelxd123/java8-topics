package com.ymt.demo.streams;

public class Dish {

    private int calories;
    private String name;
    private boolean isVegetarian;
    private DishType type;

    public Dish(String name, boolean isVegetaria, int calories, DishType type) {
        this.name = name;
        this.isVegetarian=isVegetaria;
        this.calories = calories;
        this.type = type;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DishType getType() {
        return type;
    }

    public boolean isVegetarian() {
        return isVegetarian;
    }

    public void setVegetarian(boolean vegetarian) {
        isVegetarian = vegetarian;
    }

    @Override
    public String toString() {
        return name;
    }
}
