package com.ymt.demo.topics;

public class Comerciante {

    private final String nombre;
    private final String ciudad;

    public Comerciante(String nombre, String ciudad) {
        this.nombre = nombre;
        this.ciudad = ciudad;
    }

    public String getNombre() {
        return nombre;
    }

    public String getCiudad() {
        return ciudad;
    }

    @Override
    public String toString() {
        return "Comerciante{" +
                "nombre='" + nombre + '\'' +
                '}';
    }
}
