package com.ymt.demo.topics;

public class Transaccion {

    private final Comerciante comerciante;
    private final int anio;
    private final int valor;

    public Transaccion(Comerciante comerciante, int anio, int valor) {
        this.comerciante = comerciante;
        this.anio = anio;
        this.valor = valor;
    }

    public Comerciante getComerciante() {
        return comerciante;
    }

    public int getAnio() {
        return anio;
    }

    public int getValor() {
        return valor;
    }

    @Override
    public String toString() {
        return "Transaccion{" +
                "comerciante=" + comerciante +
                ", anio=" + anio +
                ", valor=" + valor +
                '}';
    }
}
